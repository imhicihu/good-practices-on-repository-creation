
* Please, describe the event: a clear and concise description of what the issue is.
* Reproduce the steps to replicate the behavior/issue:
   - Go to `'...'`
   - Click on `'....'`
   - Scroll down to `'....'`
   - See error

* Expected behavior:  A clear and concise description of what you expected to happen.
* Screenshots (preferred) if applicable, add / share to help us to understand your problem.
* Describe your technological environment: (please complete the following information):

    - Operating System: [e.g. iOS]
    - Version [e.g. 1.0.0.0]
    
