![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# Rationale #

* The purpose of this repository is a way of checklist when we create a brand new repository.
* This repo is a living document that will grow and adapt over time

### What is this repository for? ###

+ Quick summary
    * A _systematize-to_do-checklist_ recipe when a repository is created
+ Version
    * 1.3


### How do I get set up? ###

+ Summary of set up
    - Read our latest [checklist](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/src/master/Checklist.md) in the `Source` section. 
    - There are some [snippets](https://bitbucket.org/snippets/imhicihu/) created for the ocassion
    - At some point, we will create a [milestone](https://bitbucket.org/site/master/issues/11528/make-milestones-and-versions-more). And then, enhance the walked road
      ![repository.png](https://bitbucket.org/repo/ekyaeEE/images/1675859675-repository.png)
+ Configuration
    - Depends mostly on Bitbucket, [Markdown](https://en.wikipedia.org/wiki/Markdown). In the future, can occur a migration to [Github](https://bitbucket.org/imhicihu/github-transfers/). 
    - In the meantime, some repos have been migrated/mirrored to [Github](https://github.com/imhicihu) or some documentation to [Read the docs](https://readthedocs.org/) for our inner docs and tutorials. Please, check the workflow of this migration [here](https://bitbucket.org/imhicihu/documentation-migration-to-read-the-docs-experimental)
+ Dependencies
    - _The less, the better_. A personal [motto](http://dictionary.cambridge.org/es/diccionario/ingles/motto)
+ How to run tests
    - Create an account in [Bitbucket](https://bitbucket.org/) or [Github](https://github.com/) and begin to create your project
+ Deployment instructions
    - No mandatory "_to follow_". It is a "_good practice_" exercise and follow our needs

### Contribution guidelines ###

* Writing tests
     - Fork this repo. Open an issue-pull request or just comment the workflow described. Check our [code convention](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/src/master/code_convention.md)
* Code review
     - There is no code. Just time-savers, reminders and rules of good practice to optimize _Time_ (a human creation)
* Other guidelines
     - This repo is a _massive_ guideline / [checklist](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/src/master/Checklist.md)
     - There is a [code convention](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/src/master/code_convention.md)

### Related repositories ###

* Some repositories linked with this project:
     - [One page website](https://bitbucket.org/imhicihu/one-page-website/src/)
     - [ISKM 2017](https://bitbucket.org/imhicihu/iskm2017/src/)
     - [Open Journal System (OJS) project](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/src/)
     - [Temas Medievales project](https://bitbucket.org/imhicihu/temas-medievales-project/src/)
     - [Github Transfers](https://bitbucket.org/imhicihu/github-transfers/src/)
     - [Website, et alia](https://bitbucket.org/imhicihu/website-add-ons-et-alia/src/)
     
### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
	 - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
     - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/src/master/code_of_conduct.md)


### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)